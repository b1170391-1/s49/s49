import {useState, useEffect, useContext} from "react";
//userContext - unpack the data

import {Redirect} from "react-router-dom";

import {
	Container,
	Form,
	Button
} from "react-bootstrap"

import UserContext from '../UserContext';

export default function Login () {

	const {setUser} = useContext(UserContext);

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	const [isDisabled, setIsDisabled] = useState(true)

	useEffect ( () => {
		if (email !== "" && password !== "" )
		{
			setIsDisabled(false)
		}
		else 
		{
			setIsDisabled(true)
		}
	}, [email, password])

	function loginUser(e){
		e.preventDefault()

		localStorage.setItem("email", email)

		setUser({email:localStorage.getItem('email')})

		setEmail("");
		setPassword("");

		alert("User logged in");
	}


	return (
		<Container>
			<Form 
				className="border p-3 mt-5"
				onSubmit = {
					(e) => {
						loginUser(e);
					}
				}
			>
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter Email" 
			    	value = {email}
			    	onChange={ (e) => {
			    		setEmail(e.target.value)
			    	}}
			    	/>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password"
			    	value={password}
			    	onChange = { (e) => {
			    	 	setPassword (e.target.value)
			    	 }}

			    />
			  </Form.Group>
			  <Button 
			  	variant="primary" 
			  	type="submit"
			  	disabled = {isDisabled} 
			  >
			    Submit
			  </Button>
			</Form>
		</Container>

	)
}