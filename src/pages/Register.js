import {useState, useEffect} from "react";

import {
	Container,
	Form,
	Button

} from "react-bootstrap"

import {Redirect, BrowserRouter, Switch} from "react-router-dom";


export default function Register() {

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [cp, setCp] = useState("")

	const [isDisabled, setIsDisabled] = useState(true)

	function registerUser(e){
		e.preventDefault()

		setEmail("")
		setPassword("")
		setCp("")

		alert("Thank you for registering to React Booking")
		
	}

	useEffect ( () => {
		if ((email !== "" && password !== "" && cp !== "") &&
			(password === cp))
		{
			setIsDisabled(false)

			
		}
		else 
		{
			setIsDisabled(true)
		}
	}, [email, password, cp])

	return (
	<BrowserRouter>
	 <Switch>
		<Container>
				<Form 
					className="border p-3"
					onSubmit = {
						(e) => {
								registerUser(e);
							   }
					}
				>
			{/*Email*/}
				  <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter email"
				    	value = {email}
				    	onChange={ (e) => {
				    		setEmail(e.target.value)
				    	}}
				     />
				  </Form.Group>

			  {/*password*/}
			    <Form.Group className="mb-3" controlId="formBasicPassword">
			      <Form.Label>Password</Form.Label>
			      <Form.Control type="password" placeholder="Password"
			      	 value={password}
			      	 onChange = { (e) => {
			      	 	setPassword (e.target.value)
			      	 }}
			      />
			    </Form.Group>

			  {/*Confirm Password*/}

			  <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
			    <Form.Label>Confirm Password</Form.Label>
			    <Form.Control type="password" placeholder="Confirm Password"
			    	  value = {cp}
			    	  onChange = { (e) => {
			    	  	setCp (e.target.value)
			    	  }}

			    />
			  </Form.Group>
			  	{/*Submit*/}
			    <Button 
			    	variant="primary" 
			    	type="submit"
			    	disabled = {isDisabled}
			    >
			      Submit
			    </Button>
			</Form>
		</Container>
		<Redirect to = "courses" />
	  </Switch>
	</BrowserRouter>
	)
}